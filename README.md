# Hexo-theme-yishen

### 项目介绍

*  [hexo](https://hexo.io/zh-cn/) 博客的一个主题(开发中，名称未定，暂以 `yishen` 命名)
* 布局仿照 [闪烁之狐](http://blinkfox.com/2018/09/28/qian-duan/hexo-bo-ke-zhu-ti-zhi-hexo-theme-matery-de-jie-shao/) 大佬

### 相关技术栈

* Html 部分使用 [hexo](https://hexo.io/zh-cn/) 内置的 [EJS](https://ejs.bootcss.com/)

* css样式部分使用 [less](https://less.bootcss.com/) , 通过 [vscode](https://code.visualstudio.com/) 插件 [Easy LESS](https://github.com/mrcrowl/vscode-easy-less) 编译为css

  * > 引入的是css，less只是作为快速书写工具

  * [Easy LESS](https://github.com/mrcrowl/vscode-easy-less) 插件配置项

    * ~~~json
       "less.compile": {
              "main": "../global/main.less", // 主文件相对于修改的less文件的路径
              "outExt": ".css",
              "compress": true,
              "sourceMap": false,
              "out": true
          },
      ~~~

### 框架、组件库

* 使用 [bootstrap](https://v3.bootcss.com/) 做响应式处理
* 使用 [jQuery](https://www.runoob.com/jquery/jquery-tutorial.html) 做事件处理

### 项目目录结构、及其作用

![image-20200730182332095](http://image.mdashen.com/pic/image-20200730182332095.png)



### 参考链接

* hexo主题开发的 [教程](https://www.cnblogs.com/yyhh/p/11058985.html#主题制作) 、比较全、适合入门。hexo官网的 [教程](https://hexo.io/zh-cn/docs/) 太简单了

* 部分代码参考 [blinkfox](https://github.com/blinkfox/hexo-theme-matery) 
* 搭建 hexo 博客教程: [bilibili](https://www.bilibili.com/video/BV1Yb411a7ty)

* [git教程](https://www.liaoxuefeng.com/wiki/896043488029600/896067008724000)

### 其他事项

* 网站中的配色，border颜色，背景色，模块色，等颜色！仅仅是为了区分哪一块所使用
  * 并不代表最终配色
  * 预想：设置多套配色方案，局部文件总通过less语言的特定引用
  * 在source/css/global/mixin.less中声明
  * [微信色系](https://res.wx.qq.com/a/wx_fed/wedesign/res/static/res/2b7c/WeChat_Standard_Color_Guidelines_201703.pdf)、[支付宝色系](https://opendocs.alipay.com/mini/00mpji)、[pornhub色系](#没找到参考，取色吧#FFB700)可以作为参考