// 这个js 主要用来处理 animate 动画库的应用

// 添加动画类并自动将其删除
const animateCSS = (element, animation, prefix = 'animate__') =>
  // We create a Promise and return it
  new Promise((resolve, reject) => {
    const animationName = `${prefix}${animation}`;
    const node = document.querySelector(element);

    node.classList.add(`${prefix}animated`, animationName);

    // When the animation ends, we clean the classes and resolve the Promise
    function handleAnimationEnd () {
      node.classList.remove(`${prefix}animated`, animationName);
      node.removeEventListener('animationend', handleAnimationEnd);

      resolve('Animation ended');
    }

    node.addEventListener('animationend', handleAnimationEnd);
  })
// 回到顶部按钮的进入动画
function backTopBtnEnter () {
  animateCSS('#backTopBtn', 'backInUp').then((message) => {
    // console.log('进入动画执行完毕')
  })
}
// 回到顶部按钮的退出动画
const backTopBtnOut = () =>
  new Promise((resolve, reject) => {
    animateCSS('#backTopBtn', 'backOutRight').then((message) => {
      // console.log('消失动画')
      resolve('回到顶部按钮消失动画-resolve')
    })
  })

// 移动端左侧菜单的进入动画
function mobileAsideEnter () {
  // animateCSS('.mobile-menu-wrapper', 'fadeInLeft').then()
}
// 移动端左侧菜单的消失动画
function mobileAsideOut () {
  $('.mobile-menu-wrapper').hide()
  hideMobileMenu()
  clearRollThrough()
}