// 当一个img 带有not-fancybox类名时，fancybox变不会将其显示
// 用于icon，logo，icp徽章等
// 参考链接: https://blog.ojhdt.com/20190622/fancybox/

$("img").not('.not-fancybox').each(function () {
  var element = document.createElement("a");
  $(element).attr("data-fancybox", "gallery");
  $(element).attr("href", $(this).attr("src"));
  $(this).wrap(element);
})
// 添加zoom(放大图片按钮)，会有问题(刚开始放大被禁用) http://localhost:4000/2020/07/租房-工作的副本/ 参考链接中的第一张图片
$.fancybox.defaults.buttons = ['slideShow', 'thumbs', 'close'] // 菜单上按钮组 slideShow fullScreen thumbs share download zoom close
$.fancybox.defaults.animationEffect = "zoom-in-out" //图片加载关闭效果 false zoom fade zoom-in-out
$.fancybox.defaults.transitionEffect = "slide" //图片切换效果 false fade slide circular tube zoom-in-out rotate

$('[data-fancybox="gallery"]').fancybox({
  thumbs: {},
  afterShow: function (instance, slide) {   // (参数)当前fancyBox 实例和当前的相册对象
    wh = $(window).height() / this.height;
    ww = $(window).width() / this.width;
    // 判断按照长边放大还是按照短边放大
    if (wh < ww) {
      this.height = this.height * wh;
      this.width = this.width * wh;
    } else {
      this.height = this.height * ww;
      this.width = this.width * ww;
    }
  },
  // clickContent: function (current, event) {
  //   // BUG 返回zoom没有效果，这是按照官方示例写的，代码没有错误    "zoom"  - zoom image (if loaded) 不知道这个if loaded说的具体是啥
  //   return current.type === 'image' ? 'zoom' : false
  // }
})