// TODO 点击穿透写的不满意，在找找解决方案
// BUG 返回顶部的按钮(动画)有大问题
$(document).ready(() => {
  // TODO 测试动画库 点击搜索框中的搜索图标执行 bounce效果
  $('.icon-search-wrapper').click(() => {
    $('.icon-search-wrapper').addClass("animate__animated animate__bounce")
    setTimeout(() => {
      $('.icon-search-wrapper').removeClass('animate__animated animate__bounce')
    }, 1000)
  })
  // 移动端点击左上侧菜单条
  $(".menu-btn").click(() => {
    showMobileMenu()
    rollThrough()
    mobileAsideEnter()
  })
  // 隐藏移动端菜单条
  $(".aside-wrapper").click(() => {
    mobileAsideOut()
  })
  // 防止移动端菜单条点击穿透
  $(".mobile-menu-wrapper").click((e) => {
    // TODO 能不能不写子元素事件，点击子元素默认不触发父元素呢？
    e.stopPropagation() // 防止点击传递，造成的点击穿透
  })
  // 显示目录的按钮的点击事件
  $("#tocVisibleBtn").click(() => {
    // 桌面显示器下，隐藏桌面目录(两个目录，桌面状态下目录，移动小屏下悬浮目录)
    const className = String($('#post-card').attr("class")) // 获取类名，判断是否包含col-md-9
    if (window.innerWidth > 991) {
      if (className.indexOf('col-md-9') != -1) {  // 包含该类名
        hidePcToc()
      } else {
        showPcToc()
      }
    } else {
      // 手机或平板下
      showMobileToc()
    }
  })
  // 移动设备下，单击目录信息外的蒙版层，取消显示目录
  $('.mobile-toc-mask').click(() => {
    hideMobileToc()
  })
  $(".mobile-toc").click((e) => {
    // e.stopPropagation() // 防止点击传递，造成的点击穿透
    // BUG 上面阻止点击穿透，也阻止了，目录a标签的跳转
  })
  // 返回顶部的按钮点击事件
  $('#backTopBtn').click(() => {
    backToc()
  })
  // 监听窗口大小移动
  let tocWeight = $(window).innerWidth() * 0.9 * 0.25 - 15 - 15
  $(window).resize(() => {
    // 该表目录信息外框的大小
    tocWeight = $(window).innerWidth() * 0.9 * 0.25 - 15 - 15
    $('.post-toc-wrap').width(tocWeight)
  })
  // 监听滚动条
  let tocHeight = parseInt(0) // pc端 文章也目录所在div(滚动条移动多少)
  $(window).scroll(() => {
    let scroll = $(window).scrollTop()
    // 滚动到一定位置，目录变为fixed定位，调整toc目录外框的大小，宽高
    fixedToc(scroll, tocHeight, tocWeight)
    // 判断当前页面是否包含返回顶部按钮，包含才继续执行下面代码
    if (typeof ishasTopBtn === 'undefined' || ishasTopBtn === false) {
      // console.log('未定义或者为false-> 此页面不存在返回顶部按钮')
    } else {
      // console.log('此页面存在返回顶部按钮')
      isShowTopBtn(scroll)
    }
  })
  // 点击搜索按钮，弹出搜索框
  $('#search-btn').click(() => {
    showSearch()
  })
  $('.local-search-mask').click(() => {
    hideSearch()
  })
  $('.local-search-content').click((e) => {
    e.stopPropagation()
  })
  // 点击显示打赏界面
  $('#reword-btn').click(() => {
    showReward()
    rollThrough()
    // BUG 因为清除了滚动穿透的原因(改变定位方式->fixed),导致目录模块消失
    // 返回顶部按钮消失(不影响)
  })
  $(".reward-photo-mask").click(() => {
    hideReward()
    clearRollThrough()
    showWechat()
  })
  $('.reward-photo-content').click((e) => {

    e.stopPropagation()
  })
  $('#title-wechat').click(() => {
    showWechat()
  })
  $('#title-alipay').click(() => {
    showAlipay()
  })
})
// 防止滚动穿透, 参考链接: https://juejin.im/post/6844903766982918152#heading-3
function rollThrough() {
  let scrollTop = document.body.scrollTop || document.documentElement.scrollTop
  document.body.style.cssText += 'position:fixed;width:100%;top:-' + scrollTop + 'px;'
}
// 清除防滚动穿透效果
function clearRollThrough() {
  let body = document.body
  body.style.position = ''
  let top = body.style.top
  document.body.scrollTop = document.documentElement.scrollTop = -parseInt(top)
  body.style.top = ''
}
// 返回顶部
function backToc() {
  $('body,html').animate({ scrollTop: 0 }, 400)
}
// 显示手机上的左侧菜单栏
function showMobileMenu() {
  $("#aside").show()
  $(".aside-wrapper").show()
  $(".mobile-menu-wrapper").show()
}
function hideMobileMenu() {
  // $(".mobile-menu-wrapper").hide()
  $(".aside-wrapper").hide()
  $("#aside").hide()
}
// 隐藏中等屏幕(>992px)状态下的目录
function hidePcToc() {
  $("#post-card").removeClass("col-md-9").addClass("col-md-12")
  $("#post-toc").hide()
}
// 显示中等屏幕(>992px)状态下的目录
function showPcToc() {
  $("#post-card").removeClass("col-md-12").addClass("col-md-9")
  $("#post-toc").show()
}
// 显示移动设备的toc目录
function showMobileToc() {
  $("#mobile-toc").show()
  rollThrough()
}
function hideMobileToc() {
  $("#mobile-toc").hide()
  clearRollThrough()
}
// 点击搜索按钮，弹出搜索框
function showSearch() {
  $('.local-search-wrapper').show()
  $('#search-btn').children().removeClass('iconsearch').addClass('iconclose')
  rollThrough()
}
function hideSearch() {
  $('.local-search-wrapper').hide()
  $('#search-btn').children().removeClass('iconclose').addClass('iconsearch')
  clearRollThrough()
  //  清除搜索框中内容, 以及搜索结果。下一次搜索，不在展示上一层搜索的内容！
  $('#searchInput').val("")
  $('.local-search-result').empty()
}
function showBackTopBtn() {
  // console.log('大于200，我被调用了')
  $("#backTopBtn").show()
  backTopBtnEnter()
}
function hideBackTopBtn() {
  // console.log('小于200，我被调用了，而且会被多次调用')
  backTopBtnOut().then(() => {
    // console.log('改造为异步函数')
    $("#backTopBtn").hide()
  })
}
// 显示打赏界面
function showReward() {
  $(".reward-photo-wrapper").show()
}
function hideReward() {
  $(".reward-photo-wrapper").hide()
}
function showAlipay() {
  $('#photo-alipay').show()
  $('#photo-wechat').hide()
  $('#title-alipay').css("background", "#1677ff")
  $('#title-wechat').css("background", "none")
}
function showWechat() {
  $('#photo-alipay').hide()
  $('#photo-wechat').show()
  $('#title-alipay').css("background", "none")
  $('#title-wechat').css("background", "#1AAD19")
}
// 控制在 已经引入返回顶部按钮的页面 是否显示按钮
function isShowTopBtn(scroll) {
  if (scroll > 200) {
    if ($('#backTopBtn').css('display') !== 'block') {
      showBackTopBtn()
    }
  }
  if (scroll < 200) {
    if ($('#backTopBtn').css('display') === 'block') { // 是下面代码只被执行一次
      // console.log('我还是被执行了多次') // BUG 上面这个判断不行
      // TODO 现在不影响效果，最后有空再改
      hideBackTopBtn()
    }
  }
}
// 滚动到一定位置，目录变为fixed定位，实现视差滚动(是叫这个词吧)
function fixedToc(scroll, tocHeight, tocWeight) {
  $('.post-toc-wrap').width(tocWeight)
  if (scroll > tocHeight) {
    $('.post-toc-wrap').addClass('toc-fixed')
  } else {
    $('.post-toc-wrap').removeClass('toc-fixed')
  }
}